#PINOUT FMC
#set_property PACKAGE_PIN AG8 	[get_ports LA00P]
#set_property PACKAGE_PIN AH8 	[get_ports LA00N]
#set_property PACKAGE_PIN AF7    [get_ports LA02P]
#set_property PACKAGE_PIN AF8    [get_ports LA02N]
#set_property PACKAGE_PIN AJ9 	[get_ports LA07P]
#set_property PACKAGE_PIN AH9 	[get_ports LA07N]
#set_property PACKAGE_PIN AK6 	[get_ports LA15P]
#set_property PACKAGE_PIN AK7 	[get_ports LA15N]
#set_property PACKAGE_PIN AC4 	[get_ports LA21P]
#set_property PACKAGE_PIN AB4 	[get_ports LA21N]
#set_property PACKAGE_PIN AC3    [get_ports LA28P]
#set_property PACKAGE_PIN AB3 	[get_ports LA28N]
#set_property PACKAGE_PIN AE2   [get_ports LA32P]
#set_property PACKAGE_PIN AE3   [get_ports LA32N]
#set_property PACKAGE_PIN AA12   [get_ports LA32P]
# PACKAGE_PIN AA11   [get_ports LA32N]


##PMOD P2 Signals
#set_property PACKAGE_PIN AE1 [get_ports EX_IO8]
#set_property PACKAGE_PIN AE7 [get_ports EX_IO7]
#set_property PACKAGE_PIN AE8 [get_ports EX_IO6]
#set_property PACKAGE_PIN AC12 [get_ports EX_IO5]
##PMOD P2 Direction
#set_property PACKAGE_PIN Y10 [get_ports DIR_IO8]
#set_property PACKAGE_PIN Y7 [get_ports  DIR_IO7]
#set_property PACKAGE_PIN V3 [get_ports  DIR_IO6]
#set_property PACKAGE_PIN N11 [get_ports DIR_IO5]

#VOLTAGE LEVEL

#FMC
#set_property IOSTANDARD LVCMOS18 [get_ports LA00P]
#set_property IOSTANDARD LVCMOS18 [get_ports LA00N]
#set_property IOSTANDARD LVCMOS18 [get_ports LA02P]
#set_property IOSTANDARD LVCMOS18 [get_ports LA02N]
#set_property IOSTANDARD LVCMOS18 [get_ports LA07P]
#set_property IOSTANDARD LVCMOS18 [get_ports LA07N]
#set_property IOSTANDARD LVCMOS18 [get_ports LA15P]
#set_property IOSTANDARD LVCMOS18 [get_ports LA15N]
#set_property IOSTANDARD LVCMOS18 [get_ports LA21P]
#set_property IOSTANDARD LVCMOS18 [get_ports LA21N]
#set_property IOSTANDARD LVCMOS18 [get_ports LA28P]
#set_property IOSTANDARD LVCMOS18 [get_ports LA28N]
#set_property IOSTANDARD LVCMOS18 [get_ports LA32P]
#set_property IOSTANDARD LVCMOS18 [get_ports LA32N]


##PMOD P2 Signal
#set_property IOSTANDARD LVCMOS18 [get_ports EX_IO8]
#set_property IOSTANDARD LVCMOS18 [get_ports EX_IO7]
#set_property IOSTANDARD LVCMOS18 [get_ports EX_IO6]
#set_property IOSTANDARD LVCMOS18 [get_ports EX_IO5]
##PMOD P2 Direction
#set_property IOSTANDARD LVCMOS18 [get_ports {DIR_IO8[0]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {DIR_IO7[0]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {DIR_IO6[0]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {DIR_IO5[0]}]



