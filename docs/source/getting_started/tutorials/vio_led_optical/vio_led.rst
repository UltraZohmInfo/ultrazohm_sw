==========================
Virtual Input Output (VIO) 
==========================

Aim of the tutorial
*******************

In this tutorial, the Virtual Input Output (VIO) IP-Core is used to light up LEDs on the optical adapter board.

After this tutorial, you can:

- Connect to the programmed FPGA with Vivado
- Use the VIO
- Test the optical adapter board

Requirements
************

The following tutorial requires:

- Complete UltraZohm Toolchain (Vivado, Vitis, ultrazohm_sw repository)
- UltraZohm connected to your PC by Ethernet and USB (JTAG)
- Optical adapter card in slot D3 (:ref:`dig_optical`)


UltraZohm Setup
***************

The UltraZohm has to be connected to a PC by Ethernet and USB (JTAG-Programmer) and the optical adapter card is in D3.

.. image:: ./img/vio_physical_setup.png

VIO usage
*********

.. youtube:: jtIzec7ymQA

.. image:: https://images2.imgbox.com/d3/5a/JaM3wGSv_o.gif