#ifndef SRC_IP_CORES_ADC_MODULE_LVDS_V2_IP_ADDR_H_
#define SRC_IP_CORES_ADC_MODULE_LVDS_V2_IP_ADDR_H_


#define  Conversion_Factor_1_ADC_Module_LVDS_V2_ip          0x0  	//write date register for Outport Conversion factor for ADC 1
#define  Offset_Factor_1_ADC_Module_LVDS_V2_ip              0x4  	//write date register for Outport Offset factor for ADC 1
#define  RAW_Value_1_ADC_Module_LVDS_V2_ip          		0x8  	//read data register for Inport RAW value of ADC 1
#define  RAW_Value_2_ADC_Module_LVDS_V2_ip            		0xC	  	//read data register for Inport RAW value of ADC 2
#define  RAW_Value_3_ADC_Module_LVDS_V2_ip           		0x10  	//read data register for Inport RAW value of ADC 3
#define  RAW_Value_4_ADC_Module_LVDS_V2_ip              	0x14  	//read data register for Inport RAW value of ADC 4
//#define  RAW_Value_5_ADC_Module_LVDS_V2_ip            	0x18  	//read data register for Inport RAW value of ADC 5
//#define  RAW_Value_6_ADC_Module_LVDS_V2_ip            	0x1C  	//read data register for Inport RAW value of ADC 6
//#define  RAW_Value_7_ADC_Module_LVDS_V2_ip             	0x20  	//read data register for Inport RAW value of ADC 7
//#define  RAW_Value_8_ADC_Module_LVDS_V2_ip           		0x24  	//read data register for Inport RAW value of ADC 8


#endif /* SRC_IP_CORES_ADC_MODULE_LVDS_V2_IP_ADDR_H_ */
