/******************************************************************************
* Copyright 2021 Eyke Liegmann, Sebastian Wendel
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* 
*     http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and limitations under the License.
******************************************************************************/

#ifndef GPIO_H_
#define GPIO_H_

#include "../main.h"

void Initialize_GPIO(void);
int Initialize_PS_GPIO(uint32_t gpio_base_address, uint32_t gpio_device_id);
int SetDirection_PS_GPIO(uint32_t gpio_MIO_number, uint32_t direction); // Init GPIOs
int Enable_PS_GPIO(uint32_t gpio_MIO_number, uint32_t PinSetting);
int WritePin_PS_GPIO(uint32_t gpio_MIO_number, uint32_t data);
int ReadPin_PS_GPIO(uint32_t gpio_MIO_number);
#endif /* GPIO_H_ */
